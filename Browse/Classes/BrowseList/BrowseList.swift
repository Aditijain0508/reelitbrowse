//
//  BrowseList.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation
import UIKit
import Network
import SwiftUI

public class BrowseList {
        
    public init() {}
    
    public func createBrowseListViewController() -> UIViewController {
        let viewModel = createBrowseListViewModel()
        let swiftUIViewController = UIHostingController(rootView: VideoListView(listViewModel: viewModel))
        return swiftUIViewController
    }
    
    private func createBrowseListViewModel() -> BrowseListViewModelImpl {
        let viewModel = BrowseListViewModelImpl(useCase: createBrowseListUseCase())
        return viewModel
    }
    
    private func createBrowseListUseCase() -> BrowseListUseCase {
        let useCase = BrowseListUseCaseImpl(repository: createBrowseListRepository())
        return useCase
    }
    
    private func createBrowseListRepository() -> BrowseListRepository {
        let repository = BrowseListRepositoryImpl(service: createBrowseListService())
        return repository
    }
    
    private func createBrowseListService() -> BrowseListService {
        let service = BrowseListServiceImpl()
        return service
    }
    
}

//public final class NetworkManager {}
