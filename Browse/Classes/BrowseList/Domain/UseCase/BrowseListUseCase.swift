//
//  BrowseListUseCase.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation

protocol BrowseListUseCase {
    func getProducts(completion: @escaping (Result<VideoList, Error>) -> Void)
}
