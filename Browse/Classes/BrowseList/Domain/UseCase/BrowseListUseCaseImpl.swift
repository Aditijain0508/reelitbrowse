//
//  BrowseListUseCaseImpl.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation

class BrowseListUseCaseImpl: BrowseListUseCase {
    
    private let repository: BrowseListRepository
    
    init(repository: BrowseListRepository) {
        self.repository = repository
    }
    
    func getProducts(completion: @escaping (Result<VideoList, Error>) -> Void) {
        return repository.makeServiceCallToGetProducts(completion: completion)
    }
}
