//
//  VideoList.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation

struct VideoList: Codable {
    let limit: Int
    let total: Int
    let videos: [Video]
}

struct Video: Codable, Hashable {
    let id: String
    let title: String
    let description: String
    let thumbnail: String
    let videoUrl: String
}
