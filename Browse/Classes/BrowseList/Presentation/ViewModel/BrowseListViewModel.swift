//
//  BrowseListViewModel.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation
import SwiftUI

protocol BrowseListViewModel: ObservableObject {
    var videos: [Video] { get }
    var outputDelegate: BrowseListViewModelOutput? { get set }
    func fetchProducts()
}
