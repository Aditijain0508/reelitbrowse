//
//  BrowseListViewModelOutput.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation

protocol BrowseListViewModelOutput {
    func success()
    func gotError(_ error: String)
}
