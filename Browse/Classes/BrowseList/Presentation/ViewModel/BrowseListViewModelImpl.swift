//
//  BrowseListViewModelImpl.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation
import SwiftUI
import Combine

class BrowseListViewModelImpl: ObservableObject {
    
    var outputDelegate: BrowseListViewModelOutput?
    
    @Published var videos: [Video] = []
    
    private let useCase: BrowseListUseCase
    
    func fetchProducts() {
        useCase.getProducts { [weak self] result in
            switch result {
            case .success(let videoList):
                self?.getData(model: videoList)
            case .failure(let error):
                self?.outputDelegate?.gotError(error.localizedDescription)
            }
        }
    }
    
    private func getData(model: VideoList) {
        videos = model.videos
        outputDelegate?.success()
    }
    
    init(useCase: BrowseListUseCase) {
        self.useCase = useCase
    }
    
}
