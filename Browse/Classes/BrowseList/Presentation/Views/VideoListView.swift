//
//  VideoView.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import SwiftUI
import Network
import AVKit

struct VideoView: View {
    let video: Video
    @State private var buttonTitle: String = "Subscribed now"
    
    var body: some View {
        VStack() {
            let avplayer = AVPlayer(url: URL(string: video.videoUrl)!)
            VideoPlayer(player: avplayer)
                .frame(height: 300)
                .disabled(true)
            HStack() {
                if #available(iOS 15.0, *) {
                    AsyncImage(url: URL(string: video.thumbnail)) { image in
                        image.resizable()
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: 40, height: 40)
                    .clipShape(Circle())
                    .padding()
                } else {
                    // Fallback on earlier versions
                }
                VStack(alignment: .leading, spacing: 0) {
                    Text("\(video.title)")
                        .bold()
                        .padding(5)
                    HStack(spacing: 0) {
                        Text("\(video.description)")
                            .padding(5)
                        Spacer()
                    }
                    .font(.caption)
                }
            }
            .frame(height: 60)
            .foregroundColor(.white)
            .background(Color.gray)
            .padding(.bottom)
        }
    }
}

struct VideoListView: View {
    
    @ObservedObject var listViewModel: BrowseListViewModelImpl
    
    var body: some View {
        VStack {
            List{
                ForEach(listViewModel.videos, id: \.id) { videoitem in
                    ZStack {
                        NavigationLink(destination:
                                        VideoPlayerView(video: videoitem)
                        ) {
                            EmptyView()
                        }
                        VideoView(video: videoitem)
                    }
                }
            }
            .listStyle(.plain)
            .onAppear {
                listViewModel.fetchProducts()
            }
            .navigationTitle("Reels")
        }
    }
}

struct VideoListView_Previews: PreviewProvider {
    static var previews: some View {
        let network = NetworkManager()
        let service = BrowseListServiceImpl(network: network)
        let repository = BrowseListRepositoryImpl(service: service)
        let useCase = BrowseListUseCaseImpl(repository: repository)
        let viewModel = BrowseListViewModelImpl(useCase: useCase)
        VideoListView(listViewModel: viewModel)
    }
}
