//
//  VideoPlayerView.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import AVKit
import SwiftUI

struct VideoPlayerView: View {

    let video: Video
    @State private var buttonTitle: String = "Subscribed"

    @StateObject private var playerViewModel = PlayerViewModel()

    var body: some View {
        VStack {
            ZStack {
                if let avPlayer = playerViewModel.avPlayer {
                    VideoPlayer(player: avPlayer)
                        .frame(height: 400, alignment: .top)
                }
            }.onAppear {
                playerViewModel.loadFromUrl(url: URL(string: video.videoUrl)!)
            }
            HStack() {
                if #available(iOS 15.0, *) {
                    AsyncImage(url: URL(string: video.thumbnail)) { image in
                        image.resizable()
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: 40, height: 40)
                    .clipShape(Circle())
                    .padding(.leading)
                } else {
                    // Fallback on earlier versions
                    
                }
                VStack(alignment: .leading, spacing: 0) {
                    Text("\(video.title)")
                        .bold()
                        .padding(5)
                    HStack(spacing: 0) {
                        Text("\(video.description)")
                            .padding(5)
                        Spacer()
                    }
                    .font(.caption)
                }
                Button(buttonTitle) {
                    if buttonTitle == "unsubscribed" {
                        buttonTitle = "Subscribed"
                    } else {
                        buttonTitle = "unsubscribed"
                    }
                    print("Button tapped!")
                }
                .foregroundColor(Color.red)
                .padding()
            }
            .frame(height: 60)
            .foregroundColor(.white)
            .background(Color.gray)
            .padding(.bottom)
            .navigationTitle(video.title)
            Spacer()
        }
    }
}
class PlayerViewModel: ObservableObject {
    
    @Published var avPlayer: AVPlayer?
    
    func loadFromUrl(url: URL) {
        avPlayer = AVPlayer(url: url)
        avPlayer?.play()
    }
}
