//
//  BrowseListService.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation

protocol BrowseListService {
    func makeNetworkRequest(completion: @escaping (Result<VideoList, Error>) -> Void)
}
