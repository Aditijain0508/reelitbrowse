//
//  BrowseListServiceImpl.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation
import Network

class BrowseListServiceImpl: BrowseListService {
    
    private let network: NetworkManager
    
    init(network: NetworkManager = NetworkManager()) {
        self.network = network
    }
    
    func makeNetworkRequest(completion: @escaping (Result<VideoList, Error>) -> Void) {
        var videosArray:[Video] = []
        self.network.getPosts() { result, error in
            if error == nil{
                do{
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(Videos.self, from: result!)
                    if let videos = responseModel.videos {
                        for obj in videos{
                            let video = Video(id: obj.id ?? "", title: obj.title ?? "", description: obj.videodescription ?? "", thumbnail: obj.thumbnail ?? "", videoUrl: obj.videoUrl ?? "")
                            videosArray.append(video)
                        }
                    }
                    completion(.success(VideoList(limit: 7, total: 7, videos: videosArray)))
                }
                catch{
                }
            }
        }
        
    }
    
}

public class Videos: NSObject, Decodable {
    var videos: [VideoObject]?
}

public class VideoObject: NSObject, Decodable {
    var id: String?
    var title: String?
    var videodescription: String?
    var thumbnail: String?
    var videoUrl: String?
}
