//
//  BrowseListRepository.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation

protocol BrowseListRepository {
    func makeServiceCallToGetProducts(completion: @escaping (Result<VideoList, Error>) -> Void)
}
