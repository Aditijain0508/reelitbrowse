//
//  BrowseListRepositoryImpl.swift
//  Browse
//
//  Created by Rohit Dhawan on 12/08/22.
//
import Foundation

class BrowseListRepositoryImpl: BrowseListRepository {
    
    private let service: BrowseListService
    
    init(service: BrowseListService) {
        self.service = service
    }
    
    func makeServiceCallToGetProducts(completion: @escaping (Result<VideoList, Error>) -> Void) {
        return service.makeNetworkRequest(completion: completion)
    }
    
}
