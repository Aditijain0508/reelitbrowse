//
//  ViewController.swift
//  Browse
//
//  Created by Aditi Jain 3 on 23/07/22.
//

import UIKit
import Core
import Network
import SwiftUI

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onTapOfActionButton(_ sender: Any) {
        let swiftUIViewController = BrowseList().createBrowseListViewController()
        self.navigationController?.pushViewController(swiftUIViewController, animated: true)
    }

}

