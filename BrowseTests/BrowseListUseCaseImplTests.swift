//
//  BrowseListUseCaseImplTests.swift
//  BrowseTests
//
//  Created by Rohit Dhawan on 31/07/22.
//

import Foundation
import XCTest
@testable import Browse


class BrowseListUseCaseImplTests: XCTestCase {
    
    var browseListUseCaseImpl: BrowseListUseCaseImpl!
    var browseListRepository: MockBrowseListRepository!
    var browseListViewModelOutput: MockBrowseListViewModelOutput!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        browseListViewModelOutput = MockBrowseListViewModelOutput()
        browseListRepository = MockBrowseListRepository()
        browseListUseCaseImpl = BrowseListUseCaseImpl(repository: browseListRepository)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        browseListUseCaseImpl = nil
        browseListRepository = nil
    }

    func testFetchProducts_Successs() throws {
        browseListRepository.mockGetProducts = Result.success(MockVideoList().videoList())
        browseListUseCaseImpl.getProducts { result in
            switch result {
            case .success(let videoList):
                XCTAssertTrue(videoList.videos.count > 0)
            case .failure(_):
                XCTFail("getProducts should not fail")
            }
        }
    }
    
    func testFetchProducts_Failure() throws {
        browseListUseCaseImpl.getProducts { result in
            switch result {
            case .success(_):
                XCTFail("getProducts should be fail")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
}

class MockBrowseListRepository: BrowseListRepository {
    var mockGetProducts: Result<VideoList, Error>?
    func makeServiceCallToGetProducts(completion: @escaping (Result<VideoList, Error>) -> Void) {
        completion(mockGetProducts ?? .failure(BrowseListViewModelError.emptyArray))
    }
}
