//
//  BrowseListViewModelImplTests.swift
//  BrowseTests
//
//  Created by Rohit Dhawan on 31/07/22.
//

import Foundation
import XCTest
@testable import Browse

enum BrowseListViewModelError: Error {
    case emptyArray
}
class BrowseListViewModelImplTests: XCTestCase {

    var browseListViewModel: BrowseListViewModelImpl!
    var browseListUseCase: MockBrowseListUseCase!
    var browseListViewModelOutput: MockBrowseListViewModelOutput!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        browseListViewModelOutput = MockBrowseListViewModelOutput()
        browseListUseCase = MockBrowseListUseCase()
        browseListViewModel = BrowseListViewModelImpl(useCase: browseListUseCase)
        browseListViewModel.outputDelegate = browseListViewModelOutput
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        browseListUseCase = nil
        browseListViewModel = nil
        browseListViewModelOutput = nil
    }

    func testFetchProducts_Successs() throws {
        browseListUseCase.mockGetProducts = Result.success(MockVideoList().videoList())
        browseListViewModel.fetchProducts()
        XCTAssertTrue(browseListViewModel.videos.count > 0)
    }

    func testFetchProducts_Failure() throws {
        browseListViewModel.fetchProducts()
        XCTAssertFalse(browseListViewModelOutput.methods.contains("[gotError(_:)]"))
    }
    
}

class MockBrowseListViewModelOutput: BrowseListViewModelOutput {
    var methods = [String]()
    func success() {
        methods.append(#function)
    }
    func gotError(_ error: String) {
        methods.append(#function)
    }
}

class MockBrowseListUseCase: BrowseListUseCase {
    var mockGetProducts: Result<VideoList, Error>?
    func getProducts(completion: @escaping (Result<VideoList, Error>) -> Void) {
        completion(mockGetProducts ?? .failure(BrowseListViewModelError.emptyArray))
    }
}

class MockVideoList {
    func videoList() -> VideoList {
        let video1 = Video(id: "1", title: "Title", description: "Description", thumbnail: "https://cdn.arstechnica.net/wp-content/uploads/2018/06/macOS-Mojave-Dynamic-Wallpaper-transition.jpg", videoUrl: "https://bit.ly/swswift")
        let video2 = Video(id: "2", title: "Title", description: "Description", thumbnail: "https://cdn.arstechnica.net/wp-content/uploads/2018/06/macOS-Mojave-Dynamic-Wallpaper-transition.jpg", videoUrl: "https://bit.ly/swswift")
        let video3 = Video(id: "2", title: "Title", description: "Description", thumbnail: "https://cdn.arstechnica.net/wp-content/uploads/2018/06/macOS-Mojave-Dynamic-Wallpaper-transition.jpg", videoUrl: "https://bit.ly/swswift")
        let video4 = Video(id: "2", title: "Title", description: "Description", thumbnail: "https://cdn.arstechnica.net/wp-content/uploads/2018/06/macOS-Mojave-Dynamic-Wallpaper-transition.jpg", videoUrl: "https://bit.ly/swswift")
        let video5 = Video(id: "2", title: "Title", description: "Description", thumbnail: "https://cdn.arstechnica.net/wp-content/uploads/2018/06/macOS-Mojave-Dynamic-Wallpaper-transition.jpg", videoUrl: "https://bit.ly/swswift")
        let video6 = Video(id: "2", title: "Title", description: "Description", thumbnail: "https://cdn.arstechnica.net/wp-content/uploads/2018/06/macOS-Mojave-Dynamic-Wallpaper-transition.jpg", videoUrl: "https://bit.ly/swswift")
        let video7 = Video(id: "2", title: "Title", description: "Description", thumbnail: "https://cdn.arstechnica.net/wp-content/uploads/2018/06/macOS-Mojave-Dynamic-Wallpaper-transition.jpg", videoUrl: "https://bit.ly/swswift")
        return VideoList(limit: 10, total: 7, videos: [video1, video2, video3, video4, video5, video6, video7])
    }
}
