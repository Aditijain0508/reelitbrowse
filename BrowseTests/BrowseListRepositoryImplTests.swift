//
//  BrowseListRepositoryImplTests.swift
//  BrowseTests
//
//  Created by Rohit Dhawan on 31/07/22.
//
import Foundation
import XCTest
@testable import Browse


class BrowseListRepositoryImplTests: XCTestCase {
    
    var browseListService: MockBrowseListService!
    var browseListRepositoryImpl: BrowseListRepositoryImpl!

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        browseListService = MockBrowseListService()
        browseListRepositoryImpl = BrowseListRepositoryImpl(service: browseListService)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        browseListService = nil
        browseListRepositoryImpl = nil
    }

    func testFetchProducts_Successs() throws {
        browseListService.mockGetProducts = Result.success(MockVideoList().videoList())
        browseListRepositoryImpl.makeServiceCallToGetProducts { result in
            switch result {
            case .success(let videoList):
                XCTAssertTrue(videoList.videos.count > 0)
            case .failure(_):
                XCTFail("makeServiceCallToGetProducts should not fail")
            }
        }
    }
    
    func testFetchProducts_Failure() throws {
        browseListRepositoryImpl.makeServiceCallToGetProducts { result in
            switch result {
            case .success(_):
                XCTFail("makeServiceCallToGetProducts should be fail")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
}

class MockBrowseListService: BrowseListService {
    var mockGetProducts: Result<VideoList, Error>?
    func makeNetworkRequest(completion: @escaping (Result<VideoList, Error>) -> Void) {
        completion(mockGetProducts ?? .failure(BrowseListViewModelError.emptyArray))
    }
}

